#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <map>
using namespace std;

//!	\brief Created to use LinkedList
struct LinkedListNode   
{                    
	//! @param values Used to store the values held by attributes
    string values;
	//! @param tags Created to hold tags and attributes together with . and ~ signs
    string tags;
    LinkedListNode* next;
};
//! \brief This class created to perform linkedlist operations more regularly. 
class LinkedList
{
    private:
        LinkedListNode* head;
    public:
		//! \brief The head LinkedListNode pointer was initialized in the LinkedList() constructor
        LinkedList(){head = NULL;}
		//!	\brief Add function is responsible for adding new elements to linked list. And it returns nothing
        void Add(string tag, string value)
        {
            LinkedListNode* node = new LinkedListNode();
            node->values = value;
            node->tags = tag;
            node->next = NULL;
            
            LinkedListNode** p = &head;
            while(*p != NULL)
            {
                p = &(*p)->next;
            }                
            node->next = *p;
            *p = node;
        }
        //! \brief find function is responsible from the finding the element searched in the main function
		//! \return If tag is the same with the searched string, it returns the value of the attribute, if not it returns "Not Found!"
        string find(string tag)
        {
            LinkedListNode *p = head;
            while(p != NULL)
            {
                if(p->tags == tag)
                {
                    string buffer = p->values;
                    return buffer;           
                }
                p = p->next;
            }
            return "Not Found!";
        }
};
int main() {
	//!@param n Holds the first value the user entered. n refers to the number of hrml code lines 
	//!@param q Holds the second value the user entered. q indicates the number of rows occupied by the user attributes the user wants to search
    int n, q;
    cin>>n>>q;
	//!@param str is the variable that holds the values to be retrieved from the user.
    string str;
	//! @param tags is a string vector for tags and attributes with . and ~ signs
    vector<string> tags;
	//!@param list is a variable created from the LinkedList class.
    LinkedList list;
    cin.ignore();
    
    for(int i = 0; i < n; i++)
    {
        getline(cin, str);
        str.erase(remove(str.begin(), str.end(), '<'), str.end());
        str.erase(remove(str.begin(), str.end(), '>'), str.end());
        string buffer = "";
        if(str.substr(0, 1) == "/")
        {
            tags.pop_back();
        }
        else {
            string tag, attribs, values;
			//!@param ch It is tha char variable to holds the "=" sign
            char ch;
			//!@param ss It is a variable created from the stringstream class to seperate the tags, attributes and values from the str string.
            stringstream ss;
            ss<<str;
            ss>>tag>>attribs>>ch>>values;
            string temp = "";
            
            if(tags.size() > 0)
            {
                temp = *tags.rbegin();
                temp += "." + tag;
            }
            else {
                temp = tag;
            }
            tags.push_back(temp);
            
            buffer = *tags.rbegin();
            buffer += "~" + attribs;
            values.erase(remove(values.begin(), values.end(), '"'), values.end());
            list.Add(buffer, values);
            while(ss)
            {
                ss>>attribs>>ch>>values;
                buffer = *tags.rbegin() + "~" + attribs;
                values.erase(remove(values.begin(), values.end(), '"'), values.end());
                list.Add(buffer, values);
            }
        }
    }
    for(int i = 0; i < q; i++)
    {
        stringstream ss;
        getline(cin, str);
    
        ss<<str;
        string tagg; 
        ss>>tagg;
        //!@param s It holds the expression returned from the find function defined in the LinkedList Class.
        string s;
        s = list.find(tagg);
        cout<<s<<endl;
    }
    tags.clear();
    return 0;
}