#include <bits/stdc++.h>
#include <iostream>
using namespace std;
//!\brief This method takes a number from the user and prints the value of this number to the screen. If the number is bigger than 9, then it just prints Greater than 9 to the screen
int main()
{
	//!@param n It is the number received from the user.
    int n;
    cin >> n;
    string s[] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "Greater than 9" };

    if (n < 10)
        cout << s[n - 1];
    else
        cout << s[9];
    return 0;
}
