#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int max_of_four(int a, int b, int c, int d);

int main() {
    int a, b, c, d;
    cin >> a >> b >> c >> d;
	//!@param ans It holds the number returned from the max_of_four Function
    int ans = max_of_four(a, b, c, d);
    cout << ans;

    return 0;
}
//!\brief max_of_four function is the function that find the max number between the four number entered from the user.
int max_of_four(int a, int b, int c, int d)
{
	//!@param Arr[] It is the integer array that store the four number entered by the user.
    int Arr[] = { a, b, c, d };
	
	//!@param max It is the value return from the function. It initialize with Arr[0]
    int max = Arr[0];
    for (int i = 0; i < 4; i++)
    {
        if (Arr[i] > max)
            max = Arr[i];
    }
    return max;
}
