#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int Sum(int* A, int size);
int Product(int* A, int size);
int Smallest(int* A, int size);
float Average(int sum, int size);

/**
 *\brief In main function, reading the file, saving the data obtained from the txt file to the dinamically created array and error handling operations are done
 */
int main()
{
	//! @param input_file File defined with fstream library
	fstream input_file;
	//! @param file_name To get the name of file to be opened from the user. Defined with string 
	string file_name;

	//	Request for user data entry
	cout << "Enter FileName: ";
	// Input from the user
	getline(cin, file_name);
	
	//  Used to open the input file
	input_file.open(file_name, ios::in);
	
	//	Prevents the code from crashing if the input file does NOT open
	if (!input_file.is_open())
	{
		cout << endl << "File NOT Found" << endl;
		exit(0);
	}

	/**
	 * @param size Used to create the size of the array with the first value read from the txt file. Defined with string
	 */
	string size;
	// Input from the input file
	getline(input_file, size);
	//! @param sos Used to convert the string to integer variable type. Defined with stringstream library
	stringstream sos;
	//!	@param temp The integer variable that stringstream sos will transform
	int temp;
	// Transform operation
	
	sos << size;
	sos >> skipws >> temp;

	// It works if encounters an error during the stringstream execution
	if (sos.fail())
	{
		cout << endl << "Incorrect Element Entry in txt File" << endl;
		exit(0);
	}
	else
	{
		for (int i = size.find(to_string(temp)) + (to_string(temp)).length(); i < size.length(); i++)
		{
			if (size[i] != ' ' && i < size.length())
			{
				sos << size[i];
				sos >> skipws >> temp;
				if (sos.fail())
				{
					cout << endl << "Incorrect Element Entry in txt File" << endl;
					exit(0);
				}
			}
		}
	}
	//	Clear the used stringstream sos
	sos.str("");

	//!	@param A Dynamically created array to hold data in txt file
	int* A = new int[stoi(size)];
	//! @param i Its used to count the number of elements in the txt file. Type of variable is integer  
	int i = 0;
	//! @param buffer Used to keep data read line by line from the txt file.
	string buffer;
	//! @param number Used to temporarily store the data converted to integer with stringstream ss
	int number;
	//! @param The variable used to find out which index the number is in the string
	int index;
	//! @param sizenumber Used to store the digit size of number
	int sizenumber;
	//! @param ss used to convertion string to integer variables
	stringstream ss;
	// While operation works as long as the end of the input file is not reached
	while (!input_file.eof())
	{
		// Get data from the input file with geline function 
		getline(input_file, buffer);
		// Convertion
		ss << buffer;
		ss >> number;
		// While operation works until the end of the buffer string
		while (true)
		{
			// If i is bigger than size, it gives an error message and the program will terminates
			if (i > stoi(size))
			{
				cout << endl << "Incorrect Element Entry in txt File" << endl;
				exit(0);
			}
			//	The numbers found are saved in the array
			A[i] = number;
			//	Index number of number is recorded to the index variable 
			index = buffer.find(to_string(number));
			// If index is eaual to -1 then the while loop is broken
			if (index == -1)
				break;
			// Digit size of number is recorded in the sizenumber variable
			sizenumber = (to_string(number)).length();

			// From the buffer string, the part of the number to the end of the digit is removed
			buffer.erase(0, index + sizenumber);
			//	transform operation
			ss << buffer;
			ss >> number;
			// Program terminates if transforming operation is faulty
			if (ss.fail())
			{
				cout << endl << "Incorrect Element Entry in txt File" << endl;
				//	Exit process
				exit(0);
			}
			//	i is increased
			i++;
		}
		// stringstream ss cleared
		ss.str("");
	}
	//	i is bigger or less than size, it gives an error message and terminates the program
	if (i < stoi(size) || i > stoi(size))
	{
		cout << endl << "Incorrect Element Entry in txt File" << endl;
		exit(0);
	}

	//! @param sum Get the value returned by Sum function
	int sum = Sum(A, stoi(size));
	//! @param product Get the value returned by Product function
	int product = Product(A, stoi(size));
	//! @param smallest Get the value returned by Smallest function
	int smallest = Smallest(A, stoi(size));
	//! @param average Get the value returned by Average function
	float average = Average(sum, stoi(size));

	cout << endl;
	cout << "Sum is " << sum << endl;
	cout << "Product is " << product << endl;
	cout << "Average is " << average << endl;
	cout << "Smallest is " << smallest << endl;

	input_file.close();

	cout << endl << endl << endl;
	system("pause");
	return 0;
}
/**
 *\brief Sum of elements in the array is done with the Sum Function
 *\param A is an array that holds the data from the txt file 
 *\param size is the number of elements in the array 
 */
int Sum(int* A, int size)
{
	//! @param Initialization of sum with 0
	int sum = 0; 
	for (int i = 0; i < size; i++)
	{
		sum += A[i];
	} 
	/**
	 *\return Sum of elements in the array
     */
	return sum;
}
/**
 *\brief Product of elements in the array is done with the Product Function
 *\param A is an array that holds the data from the txt file
 *\param size is the number of elements in the array
 */
int Product(int* A, int size)
{
	//! @param product Created the hold the result of the multiplication operation and initialize with 1
	//! @param i is the counter and initialize with 0 then i is increased one by one in a loop
	int product = 1, i = 0;

	while (i < size)
	{
		product *= A[i];
		i++;
	}
	/**
	 *\return Product of elements in the array
	 */
	return product;
}
/**
 *\brief Average of elements in the array is done with the Average Function
 *\param sum is the sum of elements in the array
 *\param size is the number of elements in the array
 *\return Average of elements in the array
 */
float Average(int sum, int size)
{
	return float(sum) / float(size);
}
/**
 *\brief Smallest of elements in the array is done with the Smallest Function
 *\param A is an array that holds the data from the txt file
 *\param size is the number of elements in the array
 */
int Smallest(int* A, int size)
{
	//! @param smallest Used to hold initially A[0] and then it holds the smallest element of the array
	//! @param i is the counter that start with 1 because 0 was used already
	int smallest = A[0], i = 1;

	do
	{
		if (A[i] < smallest)
			smallest = A[i];
		i++;
	} while (i < size);
	/**
	 *\return smallest of elements in the array
	 */
	return smallest;
}