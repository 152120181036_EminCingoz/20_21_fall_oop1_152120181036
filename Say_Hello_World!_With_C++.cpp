#include <iostream>
#include <cstdio>
using namespace std;
//! \brief This method prints "Hello, World!" to the screen
int main() {
    cout << "Hello, World!";
    return 0;
}