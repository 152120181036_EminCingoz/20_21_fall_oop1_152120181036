#include <stdio.h>
#include <math.h>
//!\brief update function is a function that operates with pointers and updates variables that it takes as parameters. It returns nothing because it has a void function type.
void update(int* a, int* b) {
    int c = *a;
    *a = *a + *b;
    *b = abs(c - *b);
}

int main() {
	//!@param a It is the first number entered by the user.
	//!@param b It is the second number entered by the user.
    int a, b;
	//!@param *pa It is the pointer that points the address of the variable a.
	//!@param *pb It is the pointer that points the address of the variable b.
    int* pa = &a, * pb = &b;

    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}
