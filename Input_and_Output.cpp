#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	//!@param a It is the first number received from the user
	//!@param b It is the second number received from the user
	//!@param c It is the third number received from the user
    int a, b, c;
    cin >> a >> b >> c;
	//!@param sum It is the sum of three numbers received from the user
    int sum = a + b + c;
    cout << sum;
    return 0;
}
