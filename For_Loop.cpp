#include <iostream>
#include <cstdio>
#include <string>
using namespace std;
//!\brief This method takes two numbers from the user and prints the numbers between these two numbers on the screen.
int main() {
    string str[] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
	//!@param a It is the first number.
	//!@param b It is the second number.
    int a, b;
    cin >> a;
    cin >> b;

    for (int i = a; i <= b; i++)
    {
        if (i <= 9)
            cout << str[i - 1] << endl;
        else {
            if (i % 2 == 0)
                cout << "even" << endl;
            else
                cout << "odd" << endl;
        }
    }
    return 0;
}
