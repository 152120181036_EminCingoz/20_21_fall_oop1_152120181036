#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	//!@param n It is the first number entered by the user. n represents the row of the dynamically created matrix.
	//!@param q It is the second number entered by the user. q represents the column of the dynamically created matrix.
    int n, q;
    cin >> n >> q;
	//!@param **A It is the double pointer to creat a dynamic 2D array.
    int** A = new int* [n];

    for (int i = 0; i < n; i++)
    {
        int k;
        cin >> k;
        A[i] = new int[k];
        for (int j = 0; j < k; j++)
        {
            cin >> A[i][j];
        }
    }
    int i = 0;
    while (i < q)
    {
        int k, l;
        cin >> k >> l;
        cout << A[k][l] << endl;
        i++;
    }
    return 0;
}
