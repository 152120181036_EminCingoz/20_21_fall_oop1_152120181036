#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
//!\brief parseInts function takes a string and splits that string into numbers and chars. Keeps the individual numbers with a vector class and returns this vector to the main method
vector<int> parseInts(string str) {
    vector<int> A;
    stringstream ss;
    int number;
    char ch;
    ss << str;

    while (ss >> number)
    {
        A.push_back(number);
        ss >> ch;
    }
    return A;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}
