#include <iostream>
#include <string>
using namespace std;

int main() {
    string a, b;
    cin >> a;
    cin >> b;

    int size_a = a.length(), size_b = b.length();
    printf("%d %d\n", size_a, size_b);

    cout << a + b << endl;

    char p = a[0];
    a[0] = b[0];
    b[0] = p;
    cout << a + " " + b;

    return 0;
}