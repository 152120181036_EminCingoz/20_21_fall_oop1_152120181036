//#include<bits/stdc++.h>
#include <iostream>
#include <stdio.h>
#include <cstdio>
using namespace std;
//!\brief This method prints different types of numbers received from the user to the screen
int main() {
	//!@param i integer variable
	//!@param f float variable 
	//!@param d double variable 
	//!@param l long variable 
	//!@param c char variable 
    int i;
    float f;
    double d;
    long l;
    char c;
    scanf("%d %ld %c %f %lf", &i, &l, &c, &f, &d);
    printf("%d\n%ld\n%c\n%.3f\n%.9lf\n", i, f, d, l, c);
    return 0;
}
